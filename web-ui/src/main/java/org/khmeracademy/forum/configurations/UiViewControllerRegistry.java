package org.khmeracademy.forum.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by temchannat on 7/20/17.
 */

@Configuration
public class UiViewControllerRegistry extends WebMvcConfigurerAdapter{


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // Client Site
        registry.addViewController("/").setViewName("/client/home");
        registry.addViewController("/home").setViewName("/client/home");

        //Admin Site

        registry.addViewController("/admin").setViewName("/admin/dashboard");
        registry.addViewController("/admin/dashboard").setViewName("/admin/dashboard");

    }

}
