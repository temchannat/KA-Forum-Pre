package org.khmeracademy.forum.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by temchannat on 7/20/17.
 */
@Configuration
public class UiMvcConfiguration extends WebMvcConfigurerAdapter{

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

		/*
		 * Static Resources store in the project
		 */
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("classpath:/static/");
		/*
		 * Static Resources store outside the project
		 */
        registry.addResourceHandler("/files/**")
                .addResourceLocations("file:/opt/FILES_MANAGEMENT/images/");
    }
}
