package org.khmeracademy.forum.persistence;

import org.khmeracademy.forum.domain.UserTest;

/**
 * Created by temchannat on 7/20/17.
 */
public interface UserRepository {

    UserTest findUserByEmail(String email);
}
