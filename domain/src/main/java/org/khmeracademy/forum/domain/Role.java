package org.khmeracademy.forum.domain;

import java.sql.Timestamp;

/**
 * Created by temchannat on 7/21/17.
 */
public class Role extends BaseEntity {

    public Role() {
    }

    public Role(int id, String name, String remark, String status, Timestamp createdDate, String uuid) {
        super(id, name, remark, status, createdDate, uuid);
    }
}
