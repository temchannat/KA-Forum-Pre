package org.khmeracademy.forum.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by temchannat on 7/20/17.
 */
public class UserTest {

    private int id;
    private String name;




    public UserTest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserTest(int id, String name) {
        this.id = id;
        this.name = name;
    }


}
