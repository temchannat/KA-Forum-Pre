package org.khmeracademy.forum.service.impl;

import org.khmeracademy.forum.domain.UserTest;
import org.khmeracademy.forum.persistence.UserRepository;
import org.khmeracademy.forum.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by temchannat on 7/20/17.
 */
@Service
public class UserServiceImplement implements UserService {

    UserRepository userRepository;

    @Autowired
    public UserServiceImplement(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserTest findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }
}
