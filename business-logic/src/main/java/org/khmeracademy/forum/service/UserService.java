package org.khmeracademy.forum.service;

import org.khmeracademy.forum.domain.UserTest;

/**
 * Created by temchannat on 7/20/17.
 */
public interface UserService {

    UserTest findUserByEmail(String email);
}
