package org.khmeracademy.forum.controllers.rest;

import org.khmeracademy.forum.domain.UserTest;
import org.khmeracademy.forum.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by temchannat on 7/20/17.
 */
@RestController
public class UserRestController {

    UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }
    @ResponseBody
    @RequestMapping("/user")
    public UserTest findUserById() {
        return userService.findUserByEmail("");
    }
}
