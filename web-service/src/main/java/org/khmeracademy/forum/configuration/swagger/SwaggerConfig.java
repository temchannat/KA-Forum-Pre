package org.khmeracademy.forum.configuration.swagger;


import com.wordnik.swagger.models.Contact;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;

import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by temchannat on 7/21/17.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.khmeracademy.forum.controllers.rest"))
                //.apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring RESTful Web Service")
                .description("Korea Software HRD Center - BTB 5th Generation")
                .termsOfServiceUrl("http://forum.khmeracademy.org")
                .license("License: Spring RESTful")
                .licenseUrl("http://forum.khmeracademy.org")
                .version("1.0")
//                .contact(new Contact("BTB Group 2 - 5th Generation", "http://forum.khmeracademy.org", "temchannat@gmail.com"))
                .build();
    }

}
